# Das R-Paket `sammleR` <img src="screenshot.png" align="right" width="50%" alt=""/>

**sammleR** ist ein kleines Sammelspiel für Grundschüler.
Es ist das Ergebnis eines Wochenend-Projekts mit meinen Kindern.

Man steuert eine auswählbare Figur durch das Koordinatensystem und versucht dabei
möglichst viele der kurz sichtbaren Goldmünzen, Diamanten und Kisten einzusammeln.
Zufällig auftretende Mauern erschweren die Steuerung.
Nach 90 Sekunden endet das Spiel und man kann sich mit den erreichten Punkten
in eine lokal gespeicherte Bestenliste eintragen.


## Installation in [R](https://www.R-project.org/)

```r
# install.packages("remotes")
remotes::install_gitlab("bastistician/sammleR")
```

## Spielstart

```r
library("sammleR")
los()
```

Hinweis: Das Programm nutzt folgendes Benutzerverzeichnis
um die `bestenliste()` zu speichern:
```r
tools::R_user_dir("sammleR", which = "cache")
```
Unter Linux ist das z.B. `~/.cache/R/sammleR`.

## Danksagung

- Duncan Murdoch für `getGraphicsEvents()`

- Rasmus Bååth für [**beepr**](https://CRAN.R-project.org/package=beepr)

- Philippe Grosjean für [**svDialogs**](https://www.sciviews.org/svDialogs/)
