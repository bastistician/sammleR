################################################################################
### Copyright (C) 2021 Sebastian Meyer <seb.meyer@fau.de>
################################################################################

frage_symbol <- function ()
{
    .pch <- svDialogs::dlg_list(choices = OPTS$Rs$pch,
                                title = "Wer soll sammeln?")$res
    if (length(.pch)) {
        optionen(R = OPTS$Rs[OPTS$Rs$pch == .pch,])
        if (.pch == "\U1F988") # Hai
            optionen(hintergrund = "darkturquoise")
        .pch
    } else NULL
}

frage_name <- function ()
{
    svDialogs::dlg_input("Wie heißt du? (leer lassen um nicht zu speichern)")$res
}

frage_nochmal <- function ()
{
    svDialogs::ok_cancel_box("Möchtest du erneut spielen?")
}
